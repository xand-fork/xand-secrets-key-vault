#![forbid(unsafe_code)]

use async_trait::async_trait;
use futures::{lock::Mutex, Future};
use models::{
    KeyVaultError, KeyVaultErrorResponse, ReadSecretResponse, TokenFromSharedSecretRequestBody,
    TokenResponse, TokenRetrievalErrorResponse,
};
use reqwest::StatusCode;
use serde::Deserialize;
use std::{error::Error, fmt::Debug, fmt::Display};
use thiserror::Error;
use xand_secrets::{CheckHealthError, ExposeSecret, ReadSecretError, Secret, SecretKeyValueStore};

mod models;

const DEFAULT_OAUTH_TOKEN_ENDPOINT: &str = "https://login.microsoftonline.com";

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct KeyVaultConfiguration {
    pub tenant_id: String,
    pub http_endpoint: String,
    pub client_id: String,
    pub client_secret: Secret<String>,
    pub azure_resource_id: String,
    pub custom_oauth_token_endpoint: Option<String>,
}

#[derive(Debug)]
pub struct KeyVaultSecretKeyValueStore {
    config: KeyVaultConfiguration,
    client: reqwest::Client,
    token: Mutex<Option<Secret<String>>>,
}

#[derive(Debug)]
enum TokenRetrievalError {
    Authentication {
        internal_error: Box<dyn Error + Send + Sync>,
    },
    BodyParse {
        internal_error: Box<dyn Error + Send + Sync>,
    },
    HttpResponse {
        internal_error: TokenRetrievalHttpError,
    },
    Transport {
        internal_error: Box<dyn Error + Send + Sync>,
    },
}

impl From<TokenRetrievalError> for ReadSecretError {
    fn from(e: TokenRetrievalError) -> Self {
        match e {
            TokenRetrievalError::Authentication { internal_error } => {
                ReadSecretError::Authentication { internal_error }
            }
            TokenRetrievalError::BodyParse { internal_error }
            | TokenRetrievalError::Transport { internal_error } => {
                ReadSecretError::Request { internal_error }
            }
            TokenRetrievalError::HttpResponse { internal_error } => ReadSecretError::Request {
                internal_error: Box::new(internal_error),
            },
        }
    }
}

impl From<TokenRetrievalError> for CheckHealthError {
    fn from(e: TokenRetrievalError) -> Self {
        match e {
            TokenRetrievalError::Authentication { internal_error } => {
                CheckHealthError::Authentication { internal_error }
            }
            TokenRetrievalError::BodyParse { internal_error }
            | TokenRetrievalError::Transport { internal_error } => {
                CheckHealthError::RemoteInternal { internal_error }
            }
            TokenRetrievalError::HttpResponse { internal_error } => {
                CheckHealthError::RemoteInternal {
                    internal_error: Box::new(internal_error),
                }
            }
        }
    }
}

#[derive(Debug, Error)]
pub struct TokenRetrievalHttpError {
    pub status_code: StatusCode,
    pub response: Option<TokenRetrievalErrorResponse>,
}

impl Display for TokenRetrievalHttpError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(error) = &self.response {
            write!(
                f,
                "Authentication endpoint returned status code {}. Error: {}. Description: {}.",
                self.status_code, error.error, error.error_description
            )?;
        } else {
            write!(
                f,
                "Authentication endpoint returned status code {} with an unexpected error body.",
                self.status_code
            )?;
        }

        Ok(())
    }
}

#[derive(Debug, Error)]
pub struct KeyVaultHttpError {
    pub status_code: StatusCode,
    pub response: Option<KeyVaultError>,
}

impl KeyVaultHttpError {
    async fn from_response(response: reqwest::Response) -> KeyVaultHttpError {
        KeyVaultHttpError {
            status_code: response.status(),
            response: response
                .json::<KeyVaultErrorResponse>()
                .await
                .ok()
                .map(|r| r.error),
        }
    }
}

impl Display for KeyVaultHttpError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(error) = &self.response {
            write!(
                f,
                "Key Vault returned status code {}. Error code: {}. Message: {}.",
                self.status_code, error.code, error.message
            )?;
        } else {
            write!(
                f,
                "Key Vault returned status code {} with an unexpected error body.",
                self.status_code
            )?;
        }

        Ok(())
    }
}

impl KeyVaultSecretKeyValueStore {
    pub fn create_from_config(config: KeyVaultConfiguration) -> KeyVaultSecretKeyValueStore {
        KeyVaultSecretKeyValueStore {
            config,
            client: reqwest::Client::new(),
            token: Mutex::new(None),
        }
    }

    async fn get_last_known_token(&self) -> Option<Secret<String>> {
        let token = self.token.lock().await;
        token.clone()
    }

    fn get_oauth_token_endpoint(&self) -> String {
        let token_address = self
            .config
            .custom_oauth_token_endpoint
            .as_ref()
            .map_or(DEFAULT_OAUTH_TOKEN_ENDPOINT, String::as_str);

        format!(
            "{}/{}/oauth2/token",
            token_address,
            urlencoding::encode(self.config.tenant_id.as_str())
        )
    }

    fn get_secret_read_latest_version_endpoint(&self, secret_name: &str) -> String {
        format!(
            "{}/secrets/{}?api-version=7.1",
            self.config.http_endpoint,
            urlencoding::encode(secret_name),
        )
    }

    fn get_secret_list_default_num_endpoint(&self) -> String {
        format!("{}/secrets?api-version=7.1", self.config.http_endpoint,)
    }

    async fn get_updated_token(&self) -> Result<Secret<String>, TokenRetrievalError> {
        let response = self
            .client
            .post(&self.get_oauth_token_endpoint())
            .form(&TokenFromSharedSecretRequestBody {
                grant_type: "client_credentials",
                client_id: self.config.client_id.as_str(),
                client_secret: self.config.client_secret.expose_secret(),
                resource: self.config.azure_resource_id.as_str(),
            })
            .send()
            .await
            .map_err(|e| TokenRetrievalError::Transport {
                internal_error: Box::new(e),
            })?;

        if !response.status().is_success() {
            return Err(match response.status() {
                StatusCode::UNAUTHORIZED | StatusCode::FORBIDDEN => {
                    TokenRetrievalError::Authentication {
                        internal_error: Box::new(TokenRetrievalHttpError {
                            status_code: response.status(),
                            response: response.json::<TokenRetrievalErrorResponse>().await.ok(),
                        }),
                    }
                }
                _ => TokenRetrievalError::HttpResponse {
                    internal_error: TokenRetrievalHttpError {
                        status_code: response.status(),
                        response: response.json::<TokenRetrievalErrorResponse>().await.ok(),
                    },
                },
            });
        };

        let response_body =
            response
                .json::<TokenResponse>()
                .await
                .map_err(|e| TokenRetrievalError::BodyParse {
                    internal_error: Box::new(e),
                })?;

        Ok(response_body.access_token)
    }

    async fn send_read_request_to_key_vault(
        &self,
        token: Secret<String>,
        name: &str,
    ) -> Result<Secret<String>, ReadSecretError> {
        let response = self
            .client
            .get(&self.get_secret_read_latest_version_endpoint(name))
            .bearer_auth(token.expose_secret())
            .send()
            .await
            .map_err(|e| ReadSecretError::Request {
                internal_error: Box::new(e),
            })?;

        if !response.status().is_success() {
            return Err(match response.status() {
                StatusCode::NOT_FOUND => ReadSecretError::KeyNotFound {
                    key: String::from(name),
                },
                StatusCode::UNAUTHORIZED | StatusCode::FORBIDDEN => {
                    ReadSecretError::Authentication {
                        internal_error: Box::new(KeyVaultHttpError::from_response(response).await),
                    }
                }
                _ => ReadSecretError::Request {
                    internal_error: Box::new(KeyVaultHttpError::from_response(response).await),
                },
            });
        };

        let response_body =
            response
                .json::<ReadSecretResponse>()
                .await
                .map_err(|e| ReadSecretError::Request {
                    internal_error: Box::new(e),
                })?;

        Ok(response_body.value)
    }

    async fn send_health_probe_to_key_vault(
        &self,
        token: Secret<String>,
    ) -> Result<(), CheckHealthError> {
        let response = self
            .client
            // Azure does not provide a Key Vault specific health endpoint so the list secrets
            // endpoint is used to check authenticated access to this particular Key Vault.
            .get(&self.get_secret_list_default_num_endpoint())
            .bearer_auth(token.expose_secret())
            .send()
            .await
            .map_err(|e| CheckHealthError::Unreachable {
                internal_error: Box::new(e),
            })?;

        match response.status() {
            status if status.is_success() => Ok(()),
            StatusCode::UNAUTHORIZED | StatusCode::FORBIDDEN => {
                Err(CheckHealthError::Authentication {
                    internal_error: Box::new(KeyVaultHttpError::from_response(response).await),
                })
            }
            _ => Err(CheckHealthError::RemoteInternal {
                internal_error: Box::new(KeyVaultHttpError::from_response(response).await),
            }),
        }

        // throw out the body; we just care about the HTTP status
    }

    async fn lock_and_update_token(&self) -> Result<Secret<String>, TokenRetrievalError> {
        let mut token = self.token.lock().await;
        let updated_token = self.get_updated_token().await?;
        *token = Some(updated_token.clone());

        Ok(updated_token)
    }

    async fn attempt_with_token_renewal<O, OFut, F, T, E>(
        &self,
        make_request: O,
        is_authentication_error: F,
    ) -> Result<T, E>
    where
        O: Fn(Secret<String>) -> OFut,
        OFut: Future<Output = Result<T, E>>,
        F: Fn(&E) -> bool,
        T: Debug,
        E: From<TokenRetrievalError>,
    {
        let token = self.get_last_known_token().await;
        if let Some(token_value) = token {
            let result = make_request(token_value).await;
            if !(result.is_err() && is_authentication_error(result.as_ref().unwrap_err())) {
                return result;
            }
        }

        // Fetching a token from AAD, does not invalidate existing tokens. Thus, the race conditions
        // introduced here won't cause a failure because other threads will still be able to
        // authenticate using the tokens they previously fetched. It is possible that multiple threads
        // will unnecessarily fetch new tokens independently.
        let updated_token = self.lock_and_update_token().await?;

        make_request(updated_token).await
    }
}

#[async_trait]
impl SecretKeyValueStore for KeyVaultSecretKeyValueStore {
    async fn read(&self, key: &str) -> Result<Secret<String>, ReadSecretError> {
        self.attempt_with_token_renewal(
            |token| self.send_read_request_to_key_vault(token, key),
            |e| matches!(e, ReadSecretError::Authentication { .. }),
        )
        .await
    }

    async fn check_health(&self) -> Result<(), xand_secrets::CheckHealthError> {
        self.attempt_with_token_renewal(
            |token| self.send_health_probe_to_key_vault(token),
            |e| matches!(e, CheckHealthError::Authentication { .. }),
        )
        .await
    }
}

#[cfg(test)]
mod tests;
