use crate::{KeyVaultConfiguration, KeyVaultSecretKeyValueStore, TokenRetrievalError};
use matches::assert_matches;
use mockito::{Matcher, Mock};
use secrecy::{ExposeSecret, Secret};
use xand_secrets::{ReadSecretError, SecretKeyValueStore};

fn create_basic_secret_store() -> KeyVaultSecretKeyValueStore {
    KeyVaultSecretKeyValueStore::create_from_config(KeyVaultConfiguration {
        tenant_id: String::from("test-tenant-id"),
        http_endpoint: mockito::server_url(),
        client_id: String::from("test-client-id"),
        client_secret: Secret::new(String::from("super-secret-test-secret")),
        azure_resource_id: String::from("https://vault.azure.net"),
        custom_oauth_token_endpoint: Some(mockito::server_url()),
    })
}

fn create_secret_store_invalid_http_endpoint() -> KeyVaultSecretKeyValueStore {
    KeyVaultSecretKeyValueStore::create_from_config(KeyVaultConfiguration {
        tenant_id: String::from("test-tenant-id"),
        http_endpoint: String::from("http://my.fake.url"),
        client_id: String::from("test-client-id"),
        client_secret: Secret::new(String::from("super-secret-test-secret")),
        azure_resource_id: String::from("https://vault.azure.net"),
        custom_oauth_token_endpoint: Some(mockito::server_url()),
    })
}

fn mock_token_endpoint() -> Mock {
    mockito::mock("POST", "/test-tenant-id/oauth2/token")
        .with_status(200)
        .with_body(
            r#"{
            "access_token": "my-test-access-token",
            "token_type": "Bearer",
            "expires_in": "3599",
            "expires_on": "1598657395",
            "not_before": "1598653795",
            "resource": "https://vault.azure.net"
        }"#,
        )
        .match_body(Matcher::UrlEncoded(
            String::from("grant_type"),
            String::from("client_credentials"),
        ))
        .match_body(Matcher::UrlEncoded(
            String::from("client_id"),
            String::from("test-client-id"),
        ))
        .match_body(Matcher::UrlEncoded(
            String::from("client_secret"),
            String::from("super-secret-test-secret"),
        ))
        .match_body(Matcher::UrlEncoded(
            String::from("resource"),
            String::from("https://vault.azure.net"),
        ))
}

fn mock_valid_updated_token() -> Mock {
    mockito::mock("POST", "/test-tenant-id/oauth2/token")
        .with_status(200)
        .with_body(
            r#"{
            "access_token": "my-new-test-access-token",
            "token_type": "Bearer",
            "expires_in": "3599",
            "expires_on": "1598657395",
            "not_before": "1598653795",
            "resource": "https://vault.azure.net"
        }"#,
        )
        .match_body(Matcher::UrlEncoded(
            String::from("grant_type"),
            String::from("client_credentials"),
        ))
        .match_body(Matcher::UrlEncoded(
            String::from("client_id"),
            String::from("test-client-id"),
        ))
        .match_body(Matcher::UrlEncoded(
            String::from("client_secret"),
            String::from("super-secret-test-secret"),
        ))
        .match_body(Matcher::UrlEncoded(
            String::from("resource"),
            String::from("https://vault.azure.net"),
        ))
}

fn mock_read_invalid_token_endpoint() -> Mock {
    mockito::mock("GET", "/secrets/my-secret-key")
        .with_status(401)
        .with_body(
            r#"{"error":{"code":"Unauthorized","message":"Error validating token: IDX10501"}}"#,
        )
        .match_query(Matcher::UrlEncoded(
            String::from("api-version"),
            String::from("7.1"),
        ))
        .match_header("Authorization", "Bearer my-test-access-token")
}

fn mock_list_secrets_invalid_token_endpoint() -> Mock {
    mockito::mock("GET", "/secrets")
        .with_status(401)
        .with_body(
            r#"{"error":{"code":"Unauthorized","message":"Error validating token: IDX10501"}}"#,
        )
        .match_query(Matcher::UrlEncoded(
            String::from("api-version"),
            String::from("7.1"),
        ))
        .match_header("Authorization", "Bearer my-test-access-token")
}

fn mock_secret_store_read_secret_endpoint() -> Mock {
    mockito::mock("GET", "/secrets/my-secret-key")
        .with_status(200)
        .with_body(
            r#"{
            "value": "super-secret-test-secret",
            "id": "my-secret-key"
        }"#,
        )
        .match_query(Matcher::UrlEncoded(
            String::from("api-version"),
            String::from("7.1"),
        ))
        .match_header("Authorization", "Bearer my-test-access-token")
}

fn mock_secret_store_list_secrets_endpoint() -> Mock {
    mockito::mock("GET", "/secrets")
        .with_status(200)
        // listing secrets is used to check health, body is not used
        .match_query(Matcher::UrlEncoded(
            String::from("api-version"),
            String::from("7.1"),
        ))
        .match_header("Authorization", "Bearer my-test-access-token")
}

#[tokio::test]
async fn test_valid_get_updated_token() {
    let secret_store = create_basic_secret_store();

    let mock_token_endpoint = mock_token_endpoint().expect(1).create();

    let token = secret_store.get_updated_token().await.unwrap();

    mock_token_endpoint.assert();
    assert_eq!("my-test-access-token", token.expose_secret());
}

#[tokio::test]
async fn test_get_updated_token_error_status() {
    let secret_store = create_basic_secret_store();

    let mock_token_endpoint = mockito::mock("POST", "/test-tenant-id/oauth2/token")
        .with_status(401)
        .with_body(r#"{
            "error":"invalid_client",
            "error_description":"AADSTS7000215: Invalid client secret is provided.\r\nTrace ID: abdc53e1-959f-4944-bd81-3b7d2d0ab000\r\nCorrelation ID: 8b6907a0-d96e-4c4b-8ab9-62314f23a897\r\nTimestamp: 2020-08-28 23:39:32Z",
            "error_codes":[7000215],
            "timestamp":"2020-08-28 23:39:32Z",
            "trace_id":"abdc53e1-959f-4944-bd81-3b7d2d0ab000",
            "correlation_id":"8b6907a0-d96e-4c4b-8ab9-62314f23a897",
            "error_uri":"https://login.microsoftonline.com/error?code=7000215"
        }"#)
        .expect(1)
        .create();

    let token_result = secret_store.get_updated_token().await;

    mock_token_endpoint.assert();

    if let Err(TokenRetrievalError::Authentication { internal_error }) = token_result {
        let expected_message = "\
            Authentication endpoint returned status code 401 Unauthorized. \
            Error: invalid_client. \
            Description: AADSTS7000215: Invalid client secret is provided.\r\n\
            Trace ID: abdc53e1-959f-4944-bd81-3b7d2d0ab000\r\n\
            Correlation ID: 8b6907a0-d96e-4c4b-8ab9-62314f23a897\r\n\
            Timestamp: 2020-08-28 23:39:32Z.\
        ";
        assert_eq!(expected_message, format!("{}", internal_error));
    } else {
        panic!("Expected an Authentication error, got {:?}", token_result)
    }
}

#[tokio::test]
async fn test_get_updated_token_invalid_body() {
    let secret_store = create_basic_secret_store();

    let mock_token_endpoint = mockito::mock("POST", "/test-tenant-id/oauth2/token")
        .with_status(200)
        .with_body(
            // Missing access_token field
            r#"{
            "token_type": "Bearer",
            "expires_in": "3599",
            "expires_on": "1598657395",
            "not_before": "1598653795",
            "resource": "https://vault.azure.net"
        }"#,
        )
        .expect(1)
        .create();

    let token_result = secret_store.get_updated_token().await;

    mock_token_endpoint.assert();

    if let Err(TokenRetrievalError::BodyParse { internal_error }) = token_result {
        let expected_message =
            "error decoding response body: missing field `access_token` at line 7 column 9";
        assert_eq!(expected_message, format!("{}", internal_error));
    } else {
        panic!("Expected a BodyParse error, got {:?}", token_result)
    }
}

#[tokio::test]
async fn test_get_updated_token_error_status_invalid_body() {
    let secret_store = create_basic_secret_store();

    let mock_token_endpoint = mockito::mock("POST", "/test-tenant-id/oauth2/token")
        .with_status(500)
        .with_body(r#"{ "error":"invalid_client" }"#)
        .expect(1)
        .create();

    let token_result = secret_store.get_updated_token().await;

    mock_token_endpoint.assert();

    if let Err(TokenRetrievalError::HttpResponse { internal_error }) = token_result {
        let expected_message =
            "Authentication endpoint returned status code 500 Internal Server Error with an unexpected error body.";
        assert_eq!(expected_message, format!("{}", internal_error));
    } else {
        panic!("Expected a HttpResponse error, got {:?}", token_result)
    }
}

#[tokio::test]
async fn test_valid_read() {
    let secret_store = create_basic_secret_store();
    let mock_token_endpoint = mock_token_endpoint().expect(1).create();
    let mock_secret_store_endpoint = mock_secret_store_read_secret_endpoint().expect(1).create();

    let secret = secret_store.read("my-secret-key").await.unwrap();

    mock_token_endpoint.assert();
    mock_secret_store_endpoint.assert();
    assert_eq!("super-secret-test-secret", secret.expose_secret());
}

#[tokio::test]
async fn test_key_not_found() {
    let secret_store = create_basic_secret_store();
    let mock_token_endpoint = mock_token_endpoint().expect(1).create();
    let mock_secret_store_endpoint = mockito::mock("GET", "/secrets/this-doesnt-exist")
        .with_status(404)
        .match_query(Matcher::UrlEncoded(
            String::from("api-version"),
            String::from("7.1"),
        ))
        .expect(1)
        .create();

    let secret_result = secret_store.read("this-doesnt-exist").await;

    mock_token_endpoint.assert();
    mock_secret_store_endpoint.assert();

    if let Err(ReadSecretError::KeyNotFound { key }) = secret_result {
        assert_eq!(key, "this-doesnt-exist");
    } else {
        panic!("Expected KeyNotFound error, got {:?}", secret_result);
    }
}

#[tokio::test]
async fn test_read_invalid_token() {
    let secret_store = create_basic_secret_store();
    let mock_token_endpoint = mock_token_endpoint().expect(1).create();
    let mock_secret_store_endpoint = mock_read_invalid_token_endpoint().expect(1).create();

    let secret_result = secret_store.read("my-secret-key").await;

    mock_token_endpoint.assert();
    mock_secret_store_endpoint.assert();

    let internal_error = secret_result.unwrap_err();
    let expected_message =
        "authentication with the secret store failed. Key Vault returned status code 401 Unauthorized. Error code: Unauthorized. Message: Error validating token: IDX10501.";
    assert_eq!(expected_message, format!("{}", internal_error));
}

#[tokio::test]
async fn test_request_error() {
    let secret_store = create_basic_secret_store();
    let mock_token_endpoint = mock_token_endpoint().expect(1).create();
    let mock_secret_store_endpoint = mockito::mock("GET", "/secrets/my-secret-key")
        .with_status(418)
        .with_body(r#"{"error":{"code":"Fake test error","message":"I'm a teapot"}}"#)
        .match_query(Matcher::UrlEncoded(
            String::from("api-version"),
            String::from("7.1"),
        ))
        .expect(1)
        .create();

    let secret_result = secret_store.read("my-secret-key").await;

    mock_token_endpoint.assert();
    mock_secret_store_endpoint.assert();

    let internal_error = secret_result.unwrap_err();
    let expected_message =
        "a request to the secret store returned an unhandled error. Key Vault returned status code 418 I'm a teapot. Error code: Fake test error. Message: I'm a teapot.";
    assert_eq!(expected_message, format!("{}", internal_error));
}

#[tokio::test]
async fn test_invalid_json_response() {
    let secret_store = create_basic_secret_store();
    let mock_token_endpoint = mock_token_endpoint().expect(1).create();
    let mock_secret_store_endpoint = mockito::mock("GET", "/secrets/my-secret-key")
        .with_status(200)
        .with_body(
            r#"{
            "id": "my-secret-key"
        }"#,
        )
        .match_query(Matcher::UrlEncoded(
            String::from("api-version"),
            String::from("7.1"),
        ))
        .expect(1)
        .create();

    let secret_result = secret_store.read("my-secret-key").await;

    mock_token_endpoint.assert();
    mock_secret_store_endpoint.assert();

    let internal_error = secret_result.unwrap_err();
    let expected_message =
        "a request to the secret store returned an unhandled error. error decoding response body:";
    assert!(
        format!("{}", internal_error).starts_with(expected_message),
        "Actual error: {}",
        internal_error
    );
}

#[tokio::test]
async fn test_invalid_token_endpoint() {
    let secret_store = KeyVaultSecretKeyValueStore::create_from_config(KeyVaultConfiguration {
        tenant_id: String::from("test-tenant-id"),
        http_endpoint: mockito::server_url(),
        client_id: String::from("test-client-id"),
        client_secret: Secret::new(String::from("super-secret-test-secret")),
        azure_resource_id: String::from("https://vault.azure.net"),
        custom_oauth_token_endpoint: Some(String::from("http://my.bad.token.endpoint")),
    });

    let token_result = secret_store.get_updated_token().await;

    if let Err(TokenRetrievalError::Transport { internal_error }) = token_result {
        let expected_message =
            "error sending request for url (http://my.bad.token.endpoint/test-tenant-id/oauth2/token): error trying to connect:";
        assert!(
            format!("{}", internal_error).starts_with(expected_message),
            "Actual error: {}",
            internal_error
        );
    } else {
        panic!("Expected a Transport error, got {:?}", token_result)
    }
}

#[tokio::test]
async fn test_invalid_http_endpoint() {
    let secret_store = create_secret_store_invalid_http_endpoint();
    let mock_token_endpoint = mock_token_endpoint().expect(1).create();

    let secret_result = secret_store.read("my-secret-key").await;

    mock_token_endpoint.assert();

    let internal_error = secret_result.unwrap_err();
    let expected_message =
        "a request to the secret store returned an unhandled error. error sending request for url (http://my.fake.url/secrets/my-secret-key?api-version=7.1): error trying to connect:";
    assert!(
        format!("{}", internal_error).starts_with(expected_message),
        "Actual error: {}",
        internal_error
    );
}

#[tokio::test]
async fn test_valid_read_need_to_update_token() {
    let secret_store = create_basic_secret_store();
    let mock_token_endpoint = mock_token_endpoint().expect(1).create();
    let mock_secret_store_endpoint = mock_secret_store_read_secret_endpoint().expect(1).create();

    let secret = secret_store.read("my-secret-key").await.unwrap();

    mock_token_endpoint.assert();
    mock_secret_store_endpoint.assert();
    assert_eq!("super-secret-test-secret", secret.expose_secret());

    let new_mock_token_endpoint = mock_valid_updated_token().expect(1).create();
    let old_token_mock_secret_store_endpoint =
        mock_read_invalid_token_endpoint().expect(1).create();
    let new_token_mock_secret_store_endpoint = mockito::mock("GET", "/secrets/my-secret-key")
        .with_status(200)
        .with_body(
            r#"{
            "value": "super-secret-test-secret",
            "id": "my-secret-key"
        }"#,
        )
        .match_query(Matcher::UrlEncoded(
            String::from("api-version"),
            String::from("7.1"),
        ))
        .match_header("Authorization", "Bearer my-new-test-access-token")
        .expect(1)
        .create();

    let secret = secret_store.read("my-secret-key").await.unwrap();

    new_mock_token_endpoint.assert();
    old_token_mock_secret_store_endpoint.assert();
    new_token_mock_secret_store_endpoint.assert();
    assert_eq!("super-secret-test-secret", secret.expose_secret());
}

#[test]
fn test_escape_characters_token_endpoint() {
    let secret_store = KeyVaultSecretKeyValueStore::create_from_config(KeyVaultConfiguration {
        tenant_id: String::from("invalid/test-tenant-id"),
        http_endpoint: String::from("http://test.http.endpoint"),
        client_id: String::from("test-client-id"),
        client_secret: Secret::new(String::from("super-secret-test-secret")),
        azure_resource_id: String::from("https://vault.azure.net"),
        custom_oauth_token_endpoint: Some(String::from("http://test.token.endpoint")),
    });

    let endpoint = secret_store.get_oauth_token_endpoint();

    assert_eq!(
        "http://test.token.endpoint/invalid%2Ftest-tenant-id/oauth2/token",
        endpoint.as_str()
    );
}

#[test]
fn test_escape_characters_read_endpoint() {
    let secret_store = KeyVaultSecretKeyValueStore::create_from_config(KeyVaultConfiguration {
        tenant_id: String::from("test-tenant-id"),
        http_endpoint: String::from("http://test.http.endpoint"),
        client_id: String::from("test-client-id"),
        client_secret: Secret::new(String::from("super-secret-test-secret")),
        azure_resource_id: String::from("https://vault.azure.net"),
        custom_oauth_token_endpoint: Some(String::from("http://test.token.endpoint")),
    });

    let endpoint = secret_store.get_secret_read_latest_version_endpoint("invalid/test-secret-name");

    assert_eq!(
        "http://test.http.endpoint/secrets/invalid%2Ftest-secret-name?api-version=7.1",
        endpoint.as_str()
    );
}

#[tokio::test]
async fn test_healthy() {
    let secret_store = create_basic_secret_store();
    let mock_token_endpoint = mock_token_endpoint().expect(1).create();
    let mock_secret_store_endpoint = mock_secret_store_list_secrets_endpoint().expect(1).create();

    let health_result = secret_store.check_health().await;

    mock_token_endpoint.assert();
    mock_secret_store_endpoint.assert();
    assert_matches!(health_result, Ok(()));
}

#[tokio::test]
async fn test_check_health_invalid_token() {
    let secret_store = create_basic_secret_store();
    let mock_token_endpoint = mock_token_endpoint().expect(1).create();
    let mock_secret_store_endpoint = mock_list_secrets_invalid_token_endpoint()
        .expect(1)
        .create();

    let health_result = secret_store.check_health().await;

    mock_token_endpoint.assert();
    mock_secret_store_endpoint.assert();

    let internal_error = health_result.unwrap_err();
    let expected_message =
        "authentication with the secret store failed. Key Vault returned status code 401 Unauthorized. Error code: Unauthorized. Message: Error validating token: IDX10501.";
    assert_eq!(expected_message, format!("{}", internal_error));
}

#[tokio::test]
async fn test_unreachable_health_endpoint() {
    let secret_store = create_secret_store_invalid_http_endpoint();
    let mock_token_endpoint = mock_token_endpoint().expect(1).create();

    let health_result = secret_store.check_health().await;

    mock_token_endpoint.assert();

    let internal_error = health_result.unwrap_err();
    let expected_message =
        "remote endpoint unreachable. error sending request for url (http://my.fake.url/secrets?api-version=7.1): error trying to connect:";
    assert!(
        format!("{}", internal_error).starts_with(expected_message),
        "Actual error: {}",
        internal_error
    );
}

#[tokio::test]
async fn test_healthy_need_to_update_token() {
    let secret_store = create_basic_secret_store();
    let mock_token_endpoint = mock_token_endpoint().expect(1).create();
    let mock_secret_store_endpoint = mock_secret_store_list_secrets_endpoint().expect(1).create();

    let health_result = secret_store.check_health().await;

    mock_token_endpoint.assert();
    mock_secret_store_endpoint.assert();
    assert_matches!(health_result, Ok(()));

    let new_mock_token_endpoint = mock_valid_updated_token().expect(1).create();
    let old_token_mock_secret_store_endpoint = mock_list_secrets_invalid_token_endpoint()
        .expect(1)
        .create();
    let new_token_mock_secret_store_endpoint = mockito::mock("GET", "/secrets")
        .with_status(200)
        .match_query(Matcher::UrlEncoded(
            String::from("api-version"),
            String::from("7.1"),
        ))
        .match_header("Authorization", "Bearer my-new-test-access-token")
        .expect(1)
        .create();

    let health_result = secret_store.check_health().await;

    new_mock_token_endpoint.assert();
    old_token_mock_secret_store_endpoint.assert();
    new_token_mock_secret_store_endpoint.assert();
    assert_matches!(health_result, Ok(()));
}

#[tokio::test]
async fn test_remote_internal_error() {
    let secret_store = create_basic_secret_store();
    let mock_token_endpoint = mock_token_endpoint().expect(1).create();
    let mock_secret_store_endpoint = mockito::mock("GET", "/secrets")
        .with_status(418)
        .with_body(r#"{"error":{"code":"Fake test error","message":"I'm a teapot"}}"#)
        .match_query(Matcher::UrlEncoded(
            String::from("api-version"),
            String::from("7.1"),
        ))
        .expect(1)
        .create();

    let secret_result = secret_store.check_health().await;

    mock_token_endpoint.assert();
    mock_secret_store_endpoint.assert();

    let internal_error = secret_result.unwrap_err();
    let expected_message =
        "the remote endpoint signalled an internal health issue. Key Vault returned status code 418 I'm a teapot. Error code: Fake test error. Message: I'm a teapot.";
    assert_eq!(expected_message, format!("{}", internal_error));
}
